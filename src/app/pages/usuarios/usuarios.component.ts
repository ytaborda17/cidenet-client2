import { Component } from '@angular/core';
import CustomStore from 'devextreme/data/custom_store';
import { UserService } from '../../shared/services/user.service';

@Component({
  templateUrl: 'usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})

export class UsuariosComponent {

  columns: {[key: string]: any};
  dataSource: any;
  private dxGrid: any;

  constructor(private service: UserService) {
    this.columns = [];

    this.service.formData().then(formData => {
      // setTimeout(() => { /** any questions on the setTimeout? -> @see https://blog.angular-university.io/angular-debugging/#initialimplementationofthesolution */
        this.columns = [
          {dataField: 'apellido1', caption: '1er apellido'},
          {dataField: 'apellido2', caption: '2do apellido'},
          {dataField: 'nombre1', caption: '1er nombre'},
          {dataField: 'nombre2', caption: 'Otros nombres'},
          {dataField: 'pais', caption: 'País', lookup: {displayExpr: 'nombre', valueExpr: 'id', dataSource: formData.pais}},
          {caption: 'Identificación', columns: [
            {dataField: 'identificacion_tipo', caption: 'Tipo', lookup: {displayExpr: 'nombre', valueExpr: 'id', dataSource: formData.identificacion_tipo}},
            {dataField: 'identificacion', caption: 'Número'},
          ]},
          {dataField: 'email', caption: 'Correo electrónico'},
          {dataField: 'area', caption: 'Area', groupIndex: 0, lookup: {displayExpr: 'nombre', valueExpr: 'id', dataSource: formData.area}},
          {dataField: 'ingreso', caption: 'Fecha de ingreso', dataType: 'date'},
          {dataField: 'estado', caption: 'Estado', calculateCellValue: (rowData: any) => (rowData && rowData.estado ? 'Activo' : 'Inactivo')},
          {dataField: 'registro', caption: 'Fecha de registro', dataType: 'datetime', format: 'dd/MM/yyyy HH:mm:ss'},
          {dataField: 'editado', caption: 'Fecha de última edición', dataType: 'datetime'},
        ];

      // });
    }).then(() => {
      this.dxGrid.refresh();
    });
    
    this.dataSource = new CustomStore({
      key: 'id',
      load: () => this.service.crud('GET', null, null),
      insert: (values) => this.service.crud('POST', values, null),
      update: (key, values) => this.service.crud('PUT', values, key),
      remove: (key) => this.service.crud('DELETE', null, key),
    });

  }

  onInitialized(e: any) {
    this.dxGrid = e.component;
    const pattern = {type: 'pattern', pattern: /^[A-Z]*$/, message: 'Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ.'};
    let mes_pasado = new Date();
    mes_pasado.setMonth(new Date().getMonth() - 1);

    e.component.option('editing', {
      mode: 'popup',
      allowAdding: true,
      allowDeleting: false,
      allowUpdating: true,
      popup: {
        closeOnOutsideClick: true,
        title: 'Formulario de usuarios',
        fullScreen: false,
        shadingColor: 'rgba(0,0,0,0.4)',
        showTitle: true,
      },
      selectTextOnEditStart: true,
      useIcons: true,
      form: {
        labelLocation: 'top',
        showColonAfterLabel: false,
        colCount: 3,
        items: [
          {itemType: 'group', caption: 'Datos personales', colCount: 2, colSpan: 3, items: [
            {itemType: 'group', colCount: 2, items: [
              {
                dataField: 'apellido1',
                editorOptions: {maxLength: 20},
                validationRules: [{type: 'required'}, pattern]
              },
              {
                dataField: 'apellido2',
                editorOptions: {maxLength: 20},
                validationRules: [{type: 'required'}, pattern]
              }
            ]},
            {itemType: 'group', colCount: 2, items: [
              {
                dataField: 'nombre1',
                editorOptions: {maxLength: 20},
                validationRules: [{type: 'required'}, pattern]
              },
              {
                dataField: 'nombre2',
                editorOptions: {maxLength: 20},
                validationRules: [{type: 'required'}, pattern]
              }
            ]},
          ]},
          {itemType: 'group', colCount: 2, colSpan: 2, items: [
            {dataField: 'identificacion_tipo', validationRules: [{type: 'required'}]},
            {
              dataField: 'identificacion',
              editorOptions: {maxLength: 20},
              validationRules: [{type: 'required'}, {type: 'pattern', pattern: /^[a-z0-9]+$/i, message: 'Solo permite caracteres alfanuméricos (a-z / A-Z / 0-9)'}]
            }
          ]},
          {itemType: 'empty'}, 
          {itemType: 'group', caption: 'Oficina', colCount: 3, colSpan: 3, items: [
            {dataField: 'pais', validationRules: [{type: 'required'}]},
            {dataField: 'area', validationRules: [{type: 'required'}]},
            {
              dataField: 'ingreso', editorType: 'dxDateBox', validationRules: [{ type: 'required' }], editorOptions: {
                displayFormat: 'dd/MM/yyyy',
                inputAttr: {
                  autocomplete: 'fecha-ingreso',
                  id: ''
                },
                useMaskBehavior: true,
                calendarOptions: {
                  min: mes_pasado,
                  max: new Date()
                }
              }
            },
          ]}, 
        ]
      }
    });
  }

}
