import { Component } from '@angular/core';

@Component({
  templateUrl: 'profile.component.html',
  styleUrls: [ './profile.component.scss' ]
})

export class ProfileComponent {
  employee: any;
  colCountByScreen: object;

  constructor() {
    this.employee = {
      FirstName: 'Yeniree',
      LastName: 'Taborda',
      Position: 'Desarrollador de Software',
      email: 'ytaborda17@gmail.com',
      phone: '350 751 0979',
      /* tslint:disable-next-line:max-line-length */
      Notes: 'Comencé mi experiencia como desarrollador de software hace 17 años, primero con Turbo Pacal 7.0, luego con Delphi, Power Builder y Java en la universidad y posteriormente me dedique al desarrollo Web, comence con PHP, he trabajado con C# y .NET aunque no tanto como con PHP y JavaScript, actualmente hago proyectos en Angular y Node.js con MySQL y Firebase Realtime Database. Me siento cómoda tanto en el backend como en el frontend, aunque soy más crítica en el frontend, buscando siempre, que la interfaz sea amigable para el usuario.',
    };
    this.colCountByScreen = {
      xs: 1,
      sm: 2,
      md: 3,
      lg: 4
    };
  }
}
