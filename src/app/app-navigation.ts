export const navigation = [
  {
    text: 'Usuarios',
    path: '/usuarios',
    icon: 'group'
  },
  {
    text: 'Perfil',
    path: '/perfil',
    icon: 'card'
  },
];
