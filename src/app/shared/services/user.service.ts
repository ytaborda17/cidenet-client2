import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import notify from 'devextreme/ui/notify';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient ) { }

  /**
   * @param method GET | POST | PUT | DELETE
   * @param data json object
   * @param key id del objeto
   */
  async crud(method: string, data: any, key: string) {
    let result: Observable<Object>;
    switch (method) {
      case 'GET':
        result = this.http.get(`${environment.apiUrl}/users`);
        break;
      case 'POST':
        result = this.http.post(`${environment.apiUrl}/users`, data);
        break;
      case 'PUT':
        result = this.http.put(`${environment.apiUrl}/users/${key}`, data);
        break;
      case 'DELETE':
        result = this.http.delete(`${environment.apiUrl}/users/${key}`);
        break;
    }

    try {
      const res = await result.toPromise();
      return (method === 'GET' ? res['data'] : res);
    } catch (e) {
      notify(e.error.message, 'error', 10000);
    }
  }
  
  async formData() {
    let result: Observable<Object>;
    
    result = this.http.get(`${environment.apiUrl}/users/formData`);

    try {
      const res = await result.toPromise();
      return res['data'];
    } catch (e) {
      notify(e.error.message, 'error', 10000);
    }
  }
}
