import { Component, HostBinding } from '@angular/core';
import { AuthService, ScreenService, AppInfoService } from './shared/services';
import { registerLocaleData } from '@angular/common';
import { loadMessages, locale } from 'devextreme/localization';
import config from 'devextreme/core/config';


/** To import a json file, @file tsconfig.json @requires flags "resolveJsonModule": true, "allowSyntheticDefaultImports": true */
import esMessages from 'devextreme/localization/messages/es.json';
import devexpressEsMessages from './shared/models/devexpressEsMessages.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  {
  @HostBinding('class') get getClass() {
    return Object.keys(this.screen.sizes).filter(cl => this.screen.sizes[cl]).join(' ');
  }

  constructor(private authService: AuthService, private screen: ScreenService, public appInfo: AppInfoService) {
    /**
     * Install Intl: npm i devextreme-intl
     * @see: https://js.devexpress.com/Documentation/Guide/Common/Localization/#Localize_Dates_Numbers_and_Currencies/Using_Intl
     * @see: https://raw.githubusercontent.com/DevExpress/DevExtreme/19_2/js/localization/messages/es.json
     */
    loadMessages(esMessages);
    loadMessages(devexpressEsMessages.es);
    locale('es-CO'); // locale(navigator.language);

    config({
      defaultCurrency: 'COP',
      serverDecimalSeparator: ',',
    });

    /**
     * @see: https://js.devexpress.com/Documentation/Guide/Common/Localization/#Dictionaries/Override_Strings_in_a_Dictionary
     */
    loadMessages(devexpressEsMessages);
    registerLocaleData('es'); // registerLocaleData(localeEs, 'es-CO', localeEsExtra);
  }

  isAuthenticated() {
    return this.authService.loggedIn;
  }
}
